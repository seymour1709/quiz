<?php

namespace app\controllers;

use app\models\Question;
use app\models\Answer;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $question = new Question();
        $answer = new Answer();
        return $this->render('index', ['question' => $question, 'answer' => $answer]);
    }

    public function actionArr()
    {
        $percentArr = [];
        $allAnswers = ArrayHelper::map(Answer::find()->where(['question_id' => 1])->all(), 'id', 'given');
        $sum = array_sum($allAnswers);
        foreach ($allAnswers as $key => $val) {
            $percentArr[] = [$key => [100 - ($sum - $val) * 100 / $sum]];
        }
    }

    public function actionBeginTest()
    {
        $name = Yii::$app->request->post('name');
        $email = Yii::$app->request->post('email');
        Yii::$app->session->set('name',$name);
        Yii::$app->session->set('email',$email);
        $question = Question::find()->where(['number' => 1])->one();
        $image = "/images/question/{$question->id}/image_initial.jpg";
        return json_encode(['image'=>$image]);

    }

    public function actionCheckAnswer()
    {
        $answer_id = (int)Yii::$app->request->post('answer');
        $question_id = Yii::$app->request->post('question');
        $question = Question::find()->where(['id' => $question_id])->one();

        $exp = Answer::findOne($answer_id);
        $newGiven = $exp->given = $exp->given + 1;
        Yii::$app->db->createCommand("UPDATE answer SET given=:column1 WHERE id=:id")
            ->bindValue(':id', $answer_id)
            ->bindValue(':column1', $newGiven)->execute();


        $percentArr = [];
        $allAnswers = ArrayHelper::map(Answer::find()->where(['question_id' => $question_id])->orderBy(['priority' => SORT_ASC])->all(), 'id', 'given');
        $sum = array_sum($allAnswers);
        if ($sum == 0) {
            $sum = 1;
        }
        foreach ($allAnswers as $key => $val) {
            $percentArr[] = 100 - ($sum - $val) * 100 / $sum;
        }

        $image = "/images/question/{$question->id}/image_after.jpg";
        //$image = "/images/primary.jpg";

        if ($question->correct_option_id == $answer_id) {
            return json_encode(['correct' => 'true','explanation' => $question->right_explanation, 'image' => $image, 'percent' => $percentArr]);
        } else {
            return json_encode(['correct' => 'false','explanation' => $question->explanation, 'image' => $image, 'percent' => $percentArr, 'correct' => "option-{$question->correct_option_id}"]);
        }

    }

    public function actionNextQuestion()
    {
        $number = Yii::$app->request->post('number') + 1;
        $question = Question::find()->where(['number' => $number])->one();
        if ($question) {
            //$image = "/images/question/{$question->id}/image_initial.jpg";
            $image = "/images/question/{$question->id}/image_initial.jpg";
            $ansArr = [];
            $answers = Answer::find()->where(['question_id' => $question->id])->all();
            foreach ($answers as $ans) {
                $ansArr[] = ['id' => $ans->id, 'title' => $ans->title, 'description' => $ans->description];
            }
            $arr = ['question' => $question->title, 'mid' => $question->id, 'answer' => $ansArr, 'image' => $image];
            return json_encode($arr);
        } else {
            $this->redirect(Yii::$app->urlManager->createUrl('site/finish'));
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function sendCert($email)
    {
        Yii::$app->mailer->compose('layouts/html', ['subject' => "Сертификат"])
            ->setBcc($email)
            ->setFrom(['genderquiz@prosoft.kg' => 'Квиз по тексту'])
            ->setSubject("Сертификат")
            ->setTextBody("")
            ->send();
    }

    public function actionSendEmail(){
        $email = "damirbek@gmail.com";
        Yii::$app->mailer->compose()
            ->setTo('damirbek@gmail.com')
            ->setFrom([Yii::$app->params['adminEmail'] => 'Quiz.kg'])
            ->setSubject('Call me maybe')
            ->setTextBody("Client name: ")
            ->setHtmlBody("<strong>Client name</strong>test<br /><strong>Client phone</strong>:test<br /><strong>test</strong>")
            ->send();
    }


    public function actionFinish()
    {
        $imageUrl = Url::base() . '/images/cert_bg.jpg';
        $name =  Yii::$app->session['name'];
        $email = Yii::$app->session['email'];
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $dir = Yii::getAlias("@webroot/files/{$email}");
        $string = str_replace("_", " ", $name);
        FileHelper::createDirectory($dir);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'destination' => Pdf::DEST_FILE,
            'cssFile' => 'css/cert.css',
            'filename'=>"{$dir}/certificate.pdf",
            //'cssFile' => '@web/css/cert.css',
            // any css to be embedded if required
            'cssInline' => ".certificate_bg{background-image: url({$imageUrl});background-repeat: no-repeat;background-size: cover;height:100%;width:100%}",
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'marginLeft' => 0,
            'marginTop' => 0,
            'marginRight' => 0,
            'marginBottom' => 0,
            'content' => $this->renderPartial('privacy',['name'=>$name]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                /*'SetTitle' => 'Privacy Policy - Krajee.com',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Krajee Privacy Policy||Generated On: ' . date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetAuthor' => 'Kartik Visweswaran',
                'SetCreator' => 'Kartik Visweswaran',
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',*/
            ]
        ]);

        $pdf->render();
        Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['adminEmail'] => 'Quiz.kg'])
            ->setSubject('Сертификат')
            ->setTextBody("Уважаемый {$name}")
            ->setHtmlBody("<strong>{$name}</strong>")
            ->attach("{$dir}/certificate.pdf")
            ->send();
        $this->render('finish');
    }

    public function actionReport()
    {
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('_reportView');

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@web/css/cert.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['Krajee Report Header'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
