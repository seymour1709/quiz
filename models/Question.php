<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\helpers\Json;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\helpers\Html;
use app\models\Answer;

/**
 * This is the model class for table "question".
 *
 * @property int $id
 * @property string $title
 * @property string $img_initial
 * @property string $img_result
 * @property string $video_result
 * @property int $correct_option_id
 */
class Question extends \yii\db\ActiveRecord
{

    public $email;
    public $name;
    public $file_initial;
    public $file_after;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question';
    }


    function getImage()
    {
        if (is_dir(Yii::getAlias("@webroot/images/question/{$this->id}"))) {
            $image = FileHelper::findFiles(Yii::getAlias("@webroot/images/question/{$this->id}"), [
                'recursive' => false,
                'except' => ['.gitignore'],
            ]);
            if (count($image)) {
                $baseName = basename($image[1]);
                $dir = Yii::getAlias("@webroot/images/question/{$this->id}/");
                $img = $dir.$baseName;
                $imgUrl = Yii::$app->urlManager->createUrl("/images/question/{$this->id}/{$baseName}");

                $imagine = Image::getImagine();
                $file = $imagine->open($img);
                $verticalClass = "";
                if($file->getSize()->getWidth() <= $file->getSize()->getHeight()){
                    $verticalClass = "img-vertical";
                }
                return Html::tag('div', '',
                    [
                        'class' => "MediaBlock-image {$verticalClass}",
                        //'style' => "background-image:url({$imgUrl})"
                    ]);

            } else {
                $img = Yii::$app->urlManager->createUrl('/images/site/template.png');
                return Html::tag('div', '',
                    [
                        'class' => 'MediaBlock-image',
                        'style' => "background-image:url({$img})"
                    ]);
            }
        }
    }

    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','name','email'], 'required'],
            [['img_initial', 'img_result', 'video_result', 'correct_option_id','number','explanation','right_explanation'], 'safe'],
            [['correct_option_id'], 'integer'],
            //[['email'], 'email'],
            [['title', 'img_initial', 'img_result', 'video_result'], 'string', 'max' => 255],
        ];
    }

    public function beforeValidate()
    {
        $this->file_initial = UploadedFile::getInstance($this, 'file_initial');
        $this->file_after = UploadedFile::getInstance($this, 'file_after');
        return parent::beforeValidate();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->file_initial) {
            $dir = Yii::getAlias("@webroot/images/question/{$this->id}");
            FileHelper::createDirectory($dir);

            $imagine = Image::getImagine();
            $cover_img = "image_initial." . $this->file_initial->extension;
            $this->file_initial->saveAs("{$dir}/{$cover_img}", ['quality' => 100]);

            $image = $imagine->open("{$dir}/{$cover_img}");
            $image->thumbnail(new Box(800, 600))->save("{$dir}/{$cover_img}", ['quality' => 100]);
        }

        if ($this->file_after) {
            $dir = Yii::getAlias("@webroot/images/question/{$this->id}");
            FileHelper::createDirectory($dir);

            $imagine = Image::getImagine();
            $cover_img = "image_after." . $this->file_after->extension;
            $this->file_after->saveAs("{$dir}/{$cover_img}", ['quality' => 100]);

            $image = $imagine->open("{$dir}/{$cover_img}");
            $image->thumbnail(new Box(800, 600))->save("{$dir}/{$cover_img}", ['quality' => 100]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Вопрос'),
            'img_initial' => Yii::t('app', 'Первоначальное изображение вопроса'),
            'img_result' => Yii::t('app', 'Изображение после результата'),
            'video_result' => Yii::t('app', 'Код видео после результата'),
            'correct_option_id' => Yii::t('app', 'Correct Option ID'),
            'name' => Yii::t('app', 'Ваши ФИО'),
            'email' => Yii::t('app', 'Ваш электронный адрес (сюда вышлют сертификат)'),
        ];
    }
}
