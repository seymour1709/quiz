<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Question */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-form">
    <?php
    if ($model->isNewRecord) {
        $background = "";
    } else {
        $background = $model->getImage();
    }

    if ($background == true) {
        $wallpaper = "active";
    } else {
        $wallpaper = "new";
    }
    ?>

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data'
        ]]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'file_initial')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
        ], 'pluginOptions' => [
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false

        ]
    ]);
    ?>

    <?php
    echo $form->field($model, 'file_after')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
        ], 'pluginOptions' => [
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false

        ]
    ]);
    ?>



    <?= $form->field($model, 'video_result')->textArea(['maxlength' => true]) ?>

    <?php if($model->isNewRecord):?>

    <div class="item-row clear ">
        <h1 class="main-heading">
            Варианты ответа
        </h1>
        <?php echo $form->field($answer, 'title[]')->textInput(['placeholder' => Yii::t('app', 'Вариант ответа')])->label(false); ?>

        <?php echo $form->field($answer, 'priority[]')->textInput(['placeholder' => Yii::t('app', 'Приоритет')])->label(false); ?>

        <?php echo $form->field($answer, 'description[]')->textArea(['placeholder' => Yii::t('app', 'Описание')])->label(false); ?>

        <?php echo Html::button("Удалить", ['class' => 'btn btn-clear btn-danger', 'title' => 'Удалить']); ?>
    </div>

    <?php endif;?>

    <?php
    echo $form->errorSummary($model);
    ?>

    <div class="clear"></div>

    <?= Html::button('Добавить еще', ['class' => 'btn btn-primary btn-add-item']) ?>

    <div class="clear"></div>

    <?= $form->field($model, 'correct_option_id')->hiddenInput(['value' => ''])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    var counter = 1;
    $('.btn-add-item').on('click', function () {
        counter++;
        $('.item-row:first').find("input").attr('id', 'orderitem-title' + counter);
        $('.item-row:first').clone().insertAfter('.item-row:last').find("input[type='text']").val("");
        $('.item-row:last').find(".btn-clear").addClass('btn-remove');
    });

    $('.btn-clear').click(function () {
        $(this).parents('.item-row').find("input[type='text']").val('');
    });
    $('body').on('click', '.btn-remove', function () {
        $(this).parents('.item-row').remove();
    });
</script>
