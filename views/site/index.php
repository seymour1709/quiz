<?php

use app\models\Question;
use app\models\Answer;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = 'Тест по гендерным курсам';
?>
<div class="container">
    <div class="site-index">
        <div class="general-container">

            <div class="question-form">
                <?php $form = ActiveForm::begin(['action' => ['site/generate-cert']]);
                $count_quest = Question::find()->count(); ?>
                <div class="title_heading" style="padding: 10px 20px 0px;">Тест на закрепление знаний на основе гендерных курсов</div>
                <div class="intro-form">
                    <div style="padding: 10px 20px;">
                        <div class="intro-text">
                            Вы собираетесь пройти тест на закрепление пройденного теста на гендерных курсах. Пожалуйста,
                            помогите нам сделать мир лучше, оставьте свои данные в норме ниже
                        </div>
                        <?= $form->field($question, 'name')->textInput(['maxlength' => true, 'placeholder' => $question->getAttributeLabel('name')])->label(false) ?>
                        <?= $form->field($question, 'email')->textInput(['maxlength' => true, 'placeholder' => $question->getAttributeLabel('email')])->label(false) ?>
                    </div>
                    <?= Html::img(Url::base() . '/images/primary.jpg', ['style' => 'width:100%']); ?>
                    <div class="form-group" style="margin-top: 20px;">
                        <?= Html::button('Начать тест', ['class' => 'test-begin btn-primary btn']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>


                <div class="quest-count" data-count="<?= $count_quest ?>" data-right="0"></div>
                <?php

                $question = Question::find()->where(['number' => 1])->one(); ?>
                <div class="primary-form" data-number="<?= $question->number ?>" data-id="<?= $question->id ?>">
                    <?php
                    echo $question->getImage();
                    echo Html::tag('div', '', ['class' => 'clear']);
                    echo Html::tag('h2', $question->title, ['class' => 'question-name']);

                    $answers = Answer::find()->where(['question_id' => $question->id])->orderBy(['priority' => SORT_ASC])->all(); ?>
                    <div class="answer-block">
                        <?php
                        if ($answers) {
                            foreach ($answers as $answer) {
                                echo Html::beginTag('div', [
                                    'class' => "option-hover answer-option option-$answer->id",
                                    'data-id' => $answer->id,
                                    'data-question' => $question->id
                                ]);
                                echo Html::tag('div', "<div class=ans-title-wrap>".$answer->title."</div>",
                                    [
                                        'class' => 'long-div'
                                    ]);
                                echo Html::endTag('div');
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="footer-form"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var num_quest = 1;
    var count_quest = parseInt($('.quest-count').attr('data-count'));

    $('.test-begin').on('click', function (e) {
            var name = $('#question-name').val();
            var email = $('#question-email').val();
            if (name != "" && email != "") {
                $.ajax({
                    method: "POST",
                    url: "<?=Yii::$app->urlManager->createUrl('/site/begin-test');?>",
                    data: {name: name, email: email},
                    success: function (response) {
                        var obj = JSON.parse(response);
                        $('.intro-form').fadeOut();
                        $('.primary-form').css('display', 'block');
                        $('.MediaBlock-image ').css('background-image', 'url(' + obj.image + ')');
                        $('.footer-form').text(num_quest + "/" + count_quest);

                        sessionStorage.setItem('name', name);
                        sessionStorage.setItem('email', email);
                    }
                });
            }
            else {
                $('.intro-form').append("<div class='attention'>Необходимо ввести ФИО и Email</div>")
                if (name != "") {
                    $('.field-question-name').addClass('has-error');
                }
                if (email != "") {
                    $('.field-question-email').addClass('has-error');
                }
            }
        }
    );


    $(document).on('click', '.answer-option', function () {
        if (!$(this).hasClass('checked')) {
            $('.answer-option').addClass('checked');
            $('.answer-block').addClass('checked-hover');
            var answer_id = $(this).attr('data-id');
            var thisOne = $(this);
            var question_id = $(this).attr('data-question');
            $.ajax({
                method: "POST",
                url: "<?=Yii::$app->urlManager->createUrl('/site/check-answer');?>",
                data: {answer: answer_id, question: question_id},
                success: function (response) {
                    $(".answer-option").removeClass('option-hover');
                    var obj = JSON.parse(response);
                    var fill_string =  "";
                    $(".answer-option").each(function (index) {
                        console.log(obj.percent[index]);
                        fill_string =  "to right, #87dd47 0%,#87dd47 " + obj.percent[index] + "%,#a8f371 " + obj.percent[index] + "%,#a8f371 " + (100 - parseFloat(obj.percent[index])) +"%,#a8f371 100%";
                        if(obj.correct == false){
                            fill_string = "to right, #EA7C7C 0%,#EA7C7C " + obj.percent[index] + "%,#EDAAAA " + obj.percent[index] + "%,#EDAAAA " + (100 - parseFloat(obj.percent[index])) +"%,#EDAAAA 100%";
                        }
                        //fill_string =  "to right, #b7b7b7 0%,#b7b7b7 " + obj.percent[index] + "%,#e5e5e5 " + obj.percent[index] + "%,#e5e5e5 " + (100 - parseFloat(obj.percent[index])) +"%,#e5e5e5 100%";
                        $(this).css('background', 'linear-gradient('+fill_string+')');
                        $(this).find('.long-div').append("<span class='ans-percentage'>" + Math.round(obj.percent[index]) + "%</span>");
                    });
                    //console.log(obj);
                    if (obj.correct == 'true') {
                        thisOne.css('filter','none');
                        var right_ans = parseInt($('.quest-count').attr('data-right'));
                        $('.quest-count').attr('data-right', right_ans + 1);
                        $('.primary-form').append("<div class='next-q'><h2 class=ans-true>Верно</h2><div class=ans-explanation><div style=text-align:left> " + obj.explanation + "</div><br><div class=clear>Узнайте больше, посмотрев гендерные курсы.</div><div class='forward btn btn-primary'>Далее</div></div>");

                    }
                    else {

                        var myClass = obj.correct;
                        $("." + myClass).css('filter','none');
                        thisOne.css('filter','hue-rotate(-85deg)');
                        $('.primary-form').append("<div class='next-q'><h2 class=ans-wrong>Неверно</h2><div class=ans-explanation><div style=text-align:left> " + obj.explanation + "</div><br><div class=clear>Узнайте больше, посмотрев гендерные курсы.</div><div class='forward btn btn-primary'>Далее</div></div>");
                        $('.MediaBlock-image').html('<iframe width="770" height="360" src="https://www.youtube.com/embed/videoseries?list=PLLyBDdIWhtYG3UqGLL7mXjE_BePCntUyP" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');

                    }
                }
            });

        }
    });


    $(document).on('click', '.forward', function () {
        var number = parseInt($('.primary-form').attr('data-number'));
        var count_quest = parseInt($('.quest-count').attr('data-count'));
        var right_count = parseInt($('.quest-count').attr('data-right'));
        $.ajax({
            method: "POST",
            url: "<?=Yii::$app->urlManager->createUrl('/site/next-question');?>",
            data: {number: number,right_count: right_count},
            success: function (response) {
                if (response != 'false') {
                    var obj = JSON.parse(response);
                    $('.MediaBlock-image ').css('background-image', 'url(' + obj.image + ')').html('');
                    $('.question-name').text(obj.question);
                    $('.answer-block').html('');
                    $.each(obj.answer, function (key, value) {
                        var classString = "option-hover answer-option option-" + value.id;
                        $('.answer-block').append("<div class='" + classString + "' data-id=" + value.id + " data-question=" + obj.mid + "><div class=long-div><div class=ans-title-wrap>" + value.title + "</div></div></div>");
                    });
                    $('.footer-form').text(number+1 + "/" + count_quest);
                    $('.primary-form').attr('data-number', number + 1);
                    $('.next-q').remove();
                }
                else {
                    var obj = sessionStorage.getItem('user');
                    console.log(obj);
                    $('.question-name').text("Ваш результат " + $('.quest-count').attr('data-right') + " из " + $('.quest-count').attr('data-count'));
                    $('.answer-block').remove();
                    $('.next-q').remove();
                }
            }
        });
    });
</script>


